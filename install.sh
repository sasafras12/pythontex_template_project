sudo apt-get install texlive && \
cd pythontex/ && \
latex pythontex.ins && \
sudo env PATH=$PATH python pythontex_install.py && \
cd .. && \
sudo ln ./temp/main.pdf main.pdf && \
sudo ln ./main.tex ./temp/main.tex && \
ln -s /home/ubuntu/.local/lib/python2.7/site-packages/ ~/pythontex_template_project/ && \
ln -s ~/pythontex_template_project/sympy/ ~/pythontex_template_project/temp/ && \
ln -s ~/pythontex_template_project/mpmath/ ~/pythontex_template_project/temp/ &&
sudo cp ./include/pdflatex.sublime-build /home/ubuntu/.config/sublime-text-3/Packages/User/pdflatex.sublime-build && \
sudo chmod +x ./build.sh
